from decimal import Decimal


def generator_kodow_pocztowych(kod_min='79-900', kod_max='80-155'):
    kod_min_to_int = int(kod_min.split('-')[0]) * 1000 + int(kod_min.split('-')[1])
    kod_max_to_int = int(kod_max.split('-')[0]) * 1000 + int(kod_max.split('-')[1])
    print('odp1: ', [str(i+1)[0:2] + '-' + str(i+1)[2:] for i in range(kod_min_to_int, kod_max_to_int-1)])
    return [str(i+1)[0:2] + '-' + str(i+1)[2:] for i in range(kod_min_to_int, kod_max_to_int-1)]


def wyszukiwarka_zagubionych_liczb(lista_liczb=[2,3,7,4,9], n=10):
    posortowana_lista = sorted(lista_liczb)
    lista_zagubionych = [1] if (not posortowana_lista[0] == 1) else []
    poprzednia_liczba = 1
    for aktualna_liczba in posortowana_lista:
        lista_zagubionych.extend([i+1 for i in range(poprzednia_liczba, aktualna_liczba-1)])
        poprzednia_liczba = aktualna_liczba
    print('odp2: ', lista_zagubionych)
    return lista_zagubionych


def generator_decimali(min_liczba=2, max_liczba=5.5):
    lista_decimali = [Decimal(min_liczba)]
    while min_liczba < max_liczba:
        lista_decimali.append(Decimal(min_liczba + 0.5))
        min_liczba += 0.5
    print('odp3: ', lista_decimali)
    return lista_decimali


if __name__ == '__main__':
    generator_kodow_pocztowych()
    wyszukiwarka_zagubionych_liczb()
    generator_decimali()
